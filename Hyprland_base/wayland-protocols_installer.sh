#!/bin/bash

wayland_protocols=(
"wayland-protocols-1.32"
)


# wayland-protocols v1.32 Installation

wayland_protocols_install() {
   if ls "/usr/share/pkgconfig/wayland-protocols.pc" &> /dev/null; then
     echo -e "$1 is already installed"
   else
     echo -e "$1 pkg is not installed"
     sudo cp -r "$1/usr/." "/usr"
   fi
}



# Installing the wayland-protocols-1.32


printf "\n%s - Installing $wayland_protocols .... \n"


for wayland_protocols_pkg in "$wayland_protocols"; do
    echo "Installing $wayland_protocols_pkg..."
    wayland_protocols_install "$wayland_protocols_pkg"
done


echo -e "$wayland_protocols is installed successfully..."
exit 1
