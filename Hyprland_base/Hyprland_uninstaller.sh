#!/bin/bash

# check if Hyprland is installed

if dpkg -l | grep -q "hyprland"; then
   read -p "Do you want to uninstall hyprland? (yes/no): " ans
   answer=$(echo "$ans" | tr '[:upper:]' '[:lower:]')
   if [ "$answer" = "yes" ]; then
       sudo apt autoremove hyprland
       echo "Hyprland has been uninstalled...."
  elif [ "$answer" = "no" ]; then
       echo "Uninstallation canceled...."
  else
       echo "Invalid option. Please enter 'yes' or 'no'"
  fi
else
  echo "Hyprland is not installed on this system."
fi
