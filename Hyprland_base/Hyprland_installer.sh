#!/bin/bash

Hypr_Depends=(
libgbm-dev
xwayland
kitty
scdoc
seatd
vulkan-validationlayers
vulkan-validationlayers-dev
wget
gettext
gettext-base
fontconfig
libfontconfig-dev
libffi-dev
libxml2-dev
libdrm-dev
libxkbcommon-x11-dev
libxkbregistry-dev
libxkbcommon-dev
libpixman-1-dev
libudev-dev
libseat-dev
libxcb-dri3-dev
libvulkan-dev
libvulkan-volk-dev
libvkfft-dev
libgulkan-dev
libegl-dev
libgles2
libegl1-mesa-dev
glslang-tools
libinput-bin
libinput-dev
libxcb-composite0-dev
libavutil-dev
libavcodec-dev
libavformat-dev
libxcb-ewmh2
libxcb-ewmh-dev
libxcb-present-dev
libxcb-icccm4-dev
libxcb-render-util0-dev
libxcb-res0-dev
libxcb-xinput-dev
libpango1.0-dev
xdg-desktop-portal-wlr
hwdata
libsystemd-dev
)

Hypr_Deb=(
"Hyprland_0.29.1_amd64.deb"
)

# Depends

pkg_install() {
 if sudo dpkg -s "$1" &> /dev/null ; then
   echo -e "$1 is already installed"
 else
   echo -e "Installing $1"
   sudo apt install -y "$1"
 fi
}

# Installation of the dependencies

printf "\n%s - Installing dependencies.... \n"

for pkg in "${Hypr_Depends[@]}"; do
  pkg_install "$pkg"
done



# Hyprland's Debian Package check

Deb_install() {
   if dpkg -s "hyprland" &> /dev/null; then
     echo -e "$1 is already installed"
   else
     echo -e "$1 pkg is not installed"
     sudo dpkg -i "$deb_pkg"
   fi
}


# Install Hyprland's .deb packge

printf "\n%s - Installing dependencies.... \n"

for deb_pkg in "${Hypr_Deb[@]}"; do
    echo "Installing $deb_pkg..."
    Deb_install "$deb_pkg"
done

echo -e "Hyprland v0.29.1 is installed successfully..."
exit 1
