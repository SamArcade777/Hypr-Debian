#!/bin/bash

libinput_directories=(
  "/lib/libinput"
  /lib/udev/libinput-*
  /lib/udevrules.d/90-libinput-fuzz-override*
  /lib/udev/rules.d/80-libinput-device*
  /lib/x86_64-linux-gnu/libinput*
  "/lib/x86_64-linux-gnu/pkgconfig/libinput.pc"
  "/usr/bin/libinput"
  "/usr/include/libinput.h"
  "/usr/share/libinput"
  "/usr/share/zsh/site-functions/_libinput"
)

rm_libinput() {
    for dir in "${libinput_directories[@]}"; do
        if [ "$dir" ]; then
            sudo rm -rf "$dir"
            echo "Removed: $dir"
        fi
    done
}

read -p "Do you want to uninstall libinput? (yes/no): " ans

answer=$(echo "$ans" | tr '[:upper:]' '[:lower:]')

if [ "$answer" = "yes" ]; then
    rm_libinput
    echo "Libinput directories removed."
elif [ "$answer" = "no" ]; then
    echo "Uninstallation canceled"
else
    echo "Invalid option. Please enter 'yes' or 'no' "
fi
