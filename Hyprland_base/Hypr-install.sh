#!/bin/bash

scripts=(
"Hyprland_installer.sh"
"libinput_installer.sh"
"libdisplay-info_installer.sh"
"libliftoff_installer.sh"
"wayland_installer.sh"
"wayland-protocols_installer.sh"
)


# Hypr-Debian Installer

exec_script() {
 if [ -f "$1" ]; then
   echo -e "Launching $1 with sudo..."
   sudo bash "$1"
 else
   echo -e "Error: $1 not found...."
 fi
}

# exec scripts

printf "\n%s --> Installing .... \n"

for hypr in "${scripts[@]}"; do
  exec_script "$hypr"
done
