#!/bin/bash

wayland_protocols_directories=(
  /usr/share/pkgconfig/wayland-protocols.pc
  /usr/share/wayland-protocols
)

rm_wayland_protocols() {
    for dir in "${wayland_protocols_directories[@]}"; do
        if [ "$dir" ]; then
            sudo rm -rf "$dir"
            echo "Removed: $dir"
        fi
    done
}

read -p "Do you want to uninstall wayland-protocols? (yes/no): " ans

answer=$(echo "$ans" | tr '[:upper:]' '[:lower:]')

if [ "$answer" = "yes" ]; then
    rm_wayland_protocols
    echo "Wayland-protocols directories removed."
elif [ "$answer" = "no" ]; then
    echo "Uninstallation canceled"
else
    echo "Invalid option. Please enter 'yes' or 'no' "
fi
