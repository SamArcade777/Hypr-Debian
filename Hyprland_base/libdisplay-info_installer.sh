#!/bin/bash



libdisplay_Depends=(
hwdata
)



libdisplay=(
"libdisplay-info-0.1.1"
)


# libdisplay-info Depends

pkg_install() {
 if sudo dpkg -s "$1" &> /dev/null ; then
   echo -e "$1 is already installed"
 else
   echo -e "Installing $1"
   sudo apt install -y "$1"
 fi
}


# Installation of the dependencies

printf "\n%s - Installing dependencies.... \n"


for pkg in "${libdisplay_Depends[@]}"; do
  pkg_install "$pkg"
done



# libdisplay v0.1.1 Installation


libdisplay_install() {
   if ls "/usr/lib/x86_64-linux-gnu/libdisplay-info.so" &> /dev/null; then
     echo -e "$1 is already installed"
   else
     echo -e "$1 pkg is not installed"
     sudo cp -r "$libdisplay/usr/." "/usr"
   fi
}


# Installing the Libinput

printf "\n%s - Installing $libdisplay .... \n"


for libdisplay_pkg in "$libdisplay"; do
    echo "Installing $libinput_pkg..."
    libdisplay_install "$libdisplay_pkg"
done



echo -e "$libdisplay is installed successfully..."
exit 1
