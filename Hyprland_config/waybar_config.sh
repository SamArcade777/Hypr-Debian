#!/bin/bash

waybar=(
"waybar_dracula"
)


# waybar dracula theme Installation


waybar_dracula_install() {
   if ls "$HOME/.config/waybar/config" &> /dev/null; then
     echo -e "$1 theme is already installed"
   else
     echo -e "$1 theme is not installed"
     cp -r "$1/.config/." "$HOME/.config/"
   fi
}



# Installing the waybar_dracula theme


printf "\n%s - Installing $waybar .... \n"


for waybar_theme in "$waybar"; do
    echo "Installing $waybar_theme..."
    waybar_dracula_install "$waybar_theme"
done


echo -e "$waybar is installed successfully..."
exit 1
