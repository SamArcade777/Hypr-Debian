#!/bin/bash

mako=(
"mako_dracula"
)


# mako dracula theme Installation


mako_dracula_install() {
   if ls "$HOME/.config/mako/config" &> /dev/null; then
     echo -e "$1 theme is already installed"
   else
     echo -e "$1 theme is not installed"
     cp -r "$1/.config/." "$HOME/.config/"
   fi
}



# Installing the mako_dracula theme


printf "\n%s - Installing $mako .... \n"


for mako_theme in "$mako"; do
    echo "Installing $mako_theme..."
    mako_dracula_install "$mako_theme"
done


echo -e "$mako is installed successfully..."
exit 1
