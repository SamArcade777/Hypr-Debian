#!/bin/bash

scripts=(
"fuzzel_config.sh"
"hyprland_config.sh"
"kitty_config.sh"
"mako_config.sh"
"swaylock_config.sh"
"waybar_config.sh"
)


# Hypr-Debian config Installer

exec_script() {
 if [ -f "$1" ]; then
   echo -e "Launching $1..."
   mkdir -p "$HOME/.config"
   bash "$1"
 else
   echo -e "Error: $1 not found...."
 fi
}

# exec scripts

printf "\n%s --> Installing .... \n"

for hypr in "${scripts[@]}"; do
  exec_script "$hypr"
done
