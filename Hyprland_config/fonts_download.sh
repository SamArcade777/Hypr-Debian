#!/bin/bash

mkdir Iosevka
mkdir patched-fonts
mkdir -p "$HOME/.local/share/fonts/NerdFonts"
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/Iosevka.tar.xz -P Iosevka/

tar -xvf Iosevka/Iosevka.tar.xz -C patched-fonts
