#!/bin/bash

hyprland=(
"hyprland_dracula"
)

wall=("wallpapers")

# hyprland dracula theme Installation


hyprland_dracula_install() {
   if ls "$HOME/.config/hypr/hyprland.conf" &> /dev/null; then
     echo -e "$1 theme is already installed"
   else
     echo -e "$1 theme is not installed"
     cp -r "$1/.config/." "$HOME/.config/"
     cp -r "$wall" "$HOME/"
   fi
}



# Installing the hyprland_dracula theme


printf "\n%s - Installing $hyprland .... \n"


for hyprland_theme in "$hyprland"; do
    echo "Installing $hyprland_theme..."
    hyprland_dracula_install "$hyprland_theme"
done


echo -e "$hyprland is installed successfully..."
exit 1
