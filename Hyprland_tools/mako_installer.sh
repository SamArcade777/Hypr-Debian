#!/bin/bash

mako=(
mako-1.8.0
)

# mako Installation


mako_install() {
   if ls "/usr/bin/mako" &> /dev/null; then
     echo -e "$1 is already installed"
   else
     echo -e "$1 pkg is not installed"
     sudo cp -r "$1/usr/." "/usr"
   fi
}



# Installing the mako v1.8.0


printf "\n%s - Installing $mako .... \n"


for mako_pkg in "$mako"; do
    echo "Installing $mako_pkg..."
    mako_install "$mako_pkg"
done


echo -e "$mako is installed successfully..."
exit 1


printf "\n Activating the mako service... \n"
systemctl --user enable mako
systemctl --user start mako

echo -e "$mako is installed successfully..."
exit 1
