#!/bin/bash

sddm=(
sddm
)


display_manager=(
lightdm
gdm
lxdm
lxdm-gtk3
)

# sddm installation

pkg_install() {
 if sudo dpkg -s "$1" &> /dev/null ; then
   echo -e "$1 is already installed"
 else
   echo -e "Installing $1"
   sudo apt install --no-install-recommends -y "$1"
 fi
}


printf "\n%s --> Installing $sddm.... \n"

for pkg in "${sddm[@]}"; do
  pkg_install "$pkg"
done


# Disabling the Display manager


printf "\n%s - Disabling the $display_manager .... \n"


for dm in "${display_manager[@]}"; do
    if sudo apt list installed "$dm" &> /dev/null; then
      echo -e "Disabling $dm...."
      sudo systemctl disable "$dm"
    fi
done


printf "\n Activating the sddm service... \n"
sudo systemctl enable sddm

echo -e "$sddm is installed successfully..."
exit 1
