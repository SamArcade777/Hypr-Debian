#!/bin/bash

swww=(
"swww-v0.8.1"
)


# swww Installation


swww_install() {
   if "/usr/bin/swww" --version &> /dev/null; then
     echo -e "$1 is already installed"
   else
     echo -e "$1 pkg is not installed"
     sudo cp -r "$1/usr/." "/usr"
   fi
}



# Installing the swww 0.8.1


printf "\n%s - Installing $swww .... \n"


for swww_pkg in "$swww"; do
    echo "Installing $swww_pkg..."
    swww_install "$swww_pkg"
done


echo -e "$swww is installed successfully..."
exit 1
